import tvshow
import config

tvShowFile = tvshow.getTodaysFilename()
time = tvshow.getTime()
print ("Scanning file %s for %s" % (tvShowFile, time))
row = tvshow.scanFileForTime(tvShowFile, time)

if(row):
    urlColIndex = config.csvIndexes['url']
    streamUrl = row[urlColIndex]
    print("Executing app:" + config.apppath)
    print("URL:" + streamUrl)
    try:
        tvshow.show(streamUrl)
    except Exception as e:
        print ("Something went wrong:" + str(e))
else:
    print("No show at time %s" % time)

