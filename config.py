apppath = '/Applications/VLC.app/Contents/MacOS/VLC'
filepath = '/Users/Shared/tvshow'
fileTimeFormat = "%Y_%m_%d"
hourMinuteFormat = "%H:%M"
extension = 'csv'
csvIndexes = dict(
    time = 0,
    title = 1,
    url = 2
)
appArguments = [
	'--fullscreen', 
	'--no-loop', 
	'--play-and-exit'
]