## WARNING
This is a work in progress, and just a fun little thing for my self. 
I haven't really used Python before, so this is also a little learning-project for me. 

Use it at your own risk, but feel free to modify, use and do whatever you want to the code, as long as I'm not being held responsible for anything. 

## Install
Start off by editing the cron.py file: 
Edit line 29 to point to your preferred video player. 
Edit line 39 to use whatever arguments your player must use. 

If nothing is changed, then it should work with VLC player on Mac OS High Sierra in full screen and it exits when the file has ended.

Create a cron job, that runs at every interval you need. For default purpose, every 30 minutes should be sufficient. Have the cron job execute the cron.py file

## Create tv show listings
This system works by reading a .csv file named like this; YYYY_MM_DD.csv - Use , as seperator and " as enclosure
The first row should be the time in 24 hour format, like "22:30" or "08:30". Make sure to only use intervals that match your cronjob. 
The second row should be the title of the show. 
The third row should be the url to your file. This can be either a local file, a file over your network (UNTESTED) or a file on the internet. 

Please refer to the samle file 2018_07_21.csv

## How does it work?
It's actually quite simple. The job looks through todays csv file for any row that matches the current time. If any row is found, then play the file

## Todo
Error handling, better config options, no hardcoded lines... 
