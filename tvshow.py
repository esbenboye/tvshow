from datetime import datetime
import config
import csv
import config
from subprocess import call

def getTodaysFilename():
    filename = datetime.now().strftime(config.fileTimeFormat)
    return config.filepath + "/" + filename + "." + config.extension

def getTime():
    return datetime.now().strftime(config.hourMinuteFormat)

def getFileContent(filename):
    lines = []
    with open(filename) as csvfile:
        csvContent = csv.reader(csvfile, delimiter = ',')
        for row in csvContent:
            lines.append(row)
    return lines
        
def scanFileForTime(filename, time):
    timeColIndex = config.csvIndexes['time']
    content = getFileContent(filename)
    for row in content:
        if(row[timeColIndex] == time):
            return row

def compileArguments(streamUrl):
    arguments = []
    arguments.append(config.apppath)
    arguments.append(streamUrl)
    for item in config.appArguments:
        arguments.append(item)
    return arguments

def show(streamUrl):
    arguments = compileArguments(streamUrl)
    call(arguments)
